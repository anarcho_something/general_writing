# One Struggle - My Case Against Housing Rents

## Point of This

1.  Work out precisely why I hate the practice of lording over housing.
2.  Explain to non-Anarchists why they shouldn't like it either.

## Limitations

The writer is Anarchist, but this isn't per-say an Anarchist critique of landlords or rents. At least insofar as I do not propose an Anarchist alternative and assume that people are still working for and using money.

I'm only referring to rents on housing, not commercial property or any more abstract ideas about economic rents in general.

I may not use every word quite like the reader, and my idea of what is a colloquial usage of a word may sneak through, but I'll attempt to stick to whatever dictionary.com says, I guess?

My view is, unfortunately, and thus this text will probably be, a bit "Amero-centric," for what it's worth.

## Notes

In this text, I don't use "landlord" as a pejorative but a descriptor of an entity filling a particular role in a relationship with another distinct entity as part of a larger social context. I, however, do not wish to suggest that the reader should not use "landlord" as a pejorative in daily life as I most certainly do.

The difference between large and small landlords can sometimes but usually doesn't matter.

As Sam Seder has shown, Right-Libertarians want a state, despite their endless protestations to that fact.

"Soc-Dem" refers to Social Democratic.

"ML" refers to Marxist-Leninist.

## Acknowledgments

In particular, David Graeber, Black Socialists of America (BSA), Thought Slime, and @butchanarchy had some good things to say at the right time for me to see them about this particular subject.

## The Point of Housing

The only important thing about housing is its capacity to _house_ people. Note that "to house" wraps up likely infinite potential human social relations and practices across time and geography that I won't attempt to quantify here. Housing should be judged by how well it meets all the needs of the people who do and may come to live in it.

## The Basic Argument

The only important thing about housing is its capacity to _house_ people.

There is nothing about someone living in a building that functionally requires them to pay a landlord for being the state-recognized "lord" of the land or owner of the building. It does make sense, in context, that one would pay someone else to do the actual work of constructing or repairing the building. Rent, however, or any money paid as rent that does not go towards constructing or repairing a building, does not support the actual use of the building by living humans. Rent, therefore, should not be extracted from the person(s) living in the building for them to live in it.

## Then why have landlords?

Landlords are practical for Capitalist states, particularly Neo-Liberal or Right-Libertarian forms. To the state, landlords act as bureaucrats that arbitrate who gets what housing and when. Yet, the state doesn't need to train and employ them directly. It only needs to do what it always did; maintain the police force and, this is why a Right-Libertarian or Neo-Liberal state is so compatible with having a landlord class. By contrast, a Soc-Dem or especially an ML state would supposedly implement the same relationship directly through state employees.

## Why wouldn't someone hate this?

1.  They are a petit landlord.
2.  They are a not-so-petit landlord with a not-so-petit amount of houses to their name.
3.  They own or manage a not so petit landlord company.
4.  "Capitalist Realism" or perhaps a broader "Authoritarian Realism."
5.  And sometimes related to point four, they wish to be or think they have no choice but to become a landlord.

I don't have much else to add but that Anarchists, perhaps more than other kinds of "leftest," need to articulate two things:

1.  Existing, even if imperfect, alternatives to these practices that people can do right now, even if we think certain aspects of them wouldn't continue in an Anarchist society.
2.  The point of us telling people this stuff is that we are trying to formulate new non-authoritarian social practice, and we want them to participate. We invite them to think of new ways of living and cooperating, not simply telling them how we think they should live.

## Connecting my hypothesis on Capitalism

It's an incomplete hypothesis, of course. That said, the portion relating to the state is synthesized in part from the following observation by Graeber:

"On the other hand, if one simply hands out coins to the soldiers and then demands that every family in the kingdom was obliged to pay one of those coins back to you, one would, in one blow, turn one's entire national economy into a vast machine for the provisioning of soldiers, since now every family, in order to get their hands on the coins, must find some way to contribute to the general effort to provide soldiers with things they want. Markets are brought into existence as a side effect."

David Graeber, Debt: The First 5,000 Years, Updated and Expanded

**Assertion:** The state strategy, described by Graeber above, is still ongoing in Capitalist states between the state and subjects but is abstracted and therefore obscured.

Capitalism seems to retain and yet outsource the classic monarchic state functions in such a way as to let anyone (in practice, not really _anyone,_ of course) become the tax assessor and collector. It then awards them a fraction of "kingship" (power over others) in the form of capital (i.e., Nitzan & Bichler, Capital as Power) in proportion to how good they are at extracting rents.

**To be clear:** The need to pay rent for housing is one of the ways the state aims to force you to "get your hands on the coins," just like before, but instead of giving it back to _a_ king, you give it to an _abstract_ king constituted by, in the aspect of housing, the landlord class.

## Conclusion

Paying rent for housing is terrible as it does not make housing better at fulfilling its purpose. Moreover, paying rent is part of more extensive systems of state coercion, even when carried out through "private" entities.

The BSA has good resources on what you can do right now.

In general, you should study history and analyze how people thought about how their society functioned and consciously acted to change it. Remember that you, too, are a historical actor and can analyze the social practices of the present and help develop your society into something significantly better if you want to.

Read (or listen to): Debt: The First 5,000 Years by Graeber if you want to get a sense of how much broader the possibilities for social structures have been and thus are from what you may think.

Read Capital as Power, which describes Capitalism as a mode of _power_, and I think this might be even more important than thinking in terms of "modes of production."

Land Back, ACAB, and ALAB, of course.
